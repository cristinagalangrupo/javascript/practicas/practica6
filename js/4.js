var paginacion=0;
  var ident=[];

window.addEventListener("load", () => {
    var pest=document.querySelectorAll(".pestana");
  
   pest.forEach((va)=>{
    va.addEventListener("click",(event)=>{
        var obj=event.target;
        ident=obj.getAttribute("id");
       
    })
   })
    /* **************************************************** FOTOS ****************************************************  */
    var fotos = new Array();
    var imagenes = new Array();
    var imagenesPeque= new Array();


    /* Lleno el array con las fotos */
    for (var i = 0; i < 3; i++) {
        fotos[i] = (i + 1) + ".jpg";
    }



    /* ********** AÑADIR PÁGINAS **********   */

    var centro = document.querySelector(".celda2:nth-child(2)");
    var npaginas = Math.ceil(fotos.length / 4);
    for (var i = 0; i < npaginas; i++) {
        centro.innerHTML += "<a href='#' data-indice=" + i + ">" + (i + 1) + "</a>";
    }
    var decorado=document.querySelectorAll(".celda2:nth-child(2)>a");
        decorado[paginacion].style.textDecoration="none";

    /* ********** EJECUTAR PÁGINAS **********   */
 
	var enlaces=document.querySelectorAll(".celda2 a");
    
	enlaces.forEach((va)=>{
		va.addEventListener("click",(event)=>{
            var aux = 0;
            var copia;
			var objeto=event.target;
			var indice=objeto.getAttribute("data-indice"); // a, 0, 1, s
            
            var longitud=fotos.length-1;
            /* para página anterior y siquiete*/
            switch(indice){
                case"a":
                if (paginacion>1) {
                    paginacion=paginacion-1;
                    indice=paginacion;
                }else{
                    paginacion=0;
                    indice=paginacion;
                }
                    break;
                

                case "s":
                if(paginacion<npaginas-1){
                paginacion=paginacion+1;
                indice=paginacion;
                }else{
                    paginacion=npaginas-1;
                    indice=paginacion;
                }
                 break;
            }
			

            if(parseInt(indice)>=0){
                paginacion=parseInt(indice);
            }
            for(var c=0;c<decorado.length;c++){
                decorado[c].style.textDecoration="underline";
            }
            decorado[paginacion].style.textDecoration="none";

            aux=parseInt(indice)*4;
            
	        for (var i = 0; i < 4; i++,aux++) {
                   if(aux>=longitud){
                    document.querySelectorAll(".cards")[i].src="";
                   }else{
                    document.querySelectorAll(".cards")[i].src =imagenesPeque[aux].src; 
                   } 
	        }
	    });
	});


    /* ********** Precargo imágenes grandes **********  */

    for (var i = 0; i < fotos.length; i++) {
        imagenes[i] = new Image();
        imagenes[i].src = "imgs/people/" + fotos[i];
    }

    /* **********  Precargo imágenes pequeñas **********  */

        for(var i=0;i<3;i++){
            var v;
            imagenesPeque[i]=new Image();
            imagenesPeque[i].src="imgs/people/thumbs/" + fotos[i];
        }
        for (var i = 0; i < 3; i++) {
            v=document.querySelectorAll(".cards")[i].src = imagenesPeque[i].src;
        }



    /* ********** imagen por defecto ********** */

    document.querySelector(".mediana").src = imagenes[0].src;

    /* **********  EJECUTAR 1 ********** */

    for (var i = 0; i < 4; i++) {
        document.querySelectorAll(".cards")[i].onclick = ejecutar1;
    }
/* ********** Poner imagen mediana al hacer click **********   */
    function ejecutar1(event) {
        var indice = event.target.getAttribute("data-indice")*1;
        var caja = document.querySelector(".foto");
        indice=paginacion*4+indice;
        caja.style.backgroundImage = "url('" + imagenes[indice].src + "')";
        caja.style.backgroundSize = "100%";
        document.querySelector(".mediana").src = imagenes[indice].src;

    }


    /* ********** Poner imagen grande al hacer click **********   */

    document.querySelector(".foto").onclick = ejecutar2;
    document.querySelector(".ocultar").style.display = "none";
    document.querySelector(".ocultar").onclick = cerrar;

   
    function ejecutar2(event) {
        var indice = event.target.getAttribute("data-indice");
        var caja = document.querySelector(".ocultar");
        caja.style.backgroundImage = "url('" + document.querySelector(".mediana").src + "')";
        caja.style.backgroundSize="75%";

        caja.style.display = "block";
    }
    /* **********  Cerrar **********  */
    function cerrar() {
        document.querySelector(".ocultar").style.display = "none";
        document.querySelector(".ocultar").style.backgroundImage = "none";
    }


});