var paginacion=0;
window.addEventListener("load", () => {
    /* **************************************************** FLORES ****************************************************  */
    var fotos = new Array();
    var imagenes = new Array();
    var imagenesPeque= new Array();


    /* Lleno el array con las fotos */
    for (var i = 0; i < 10; i++) {
        fotos[i] = (i + 1) + ".jpg";
    }


    /* ********** AÑADIR PÁGINAS **********   */

    var centro = document.querySelector(".celda2:nth-child(2)");
    var npaginas = Math.ceil(fotos.length / 4);
    for (var i = 0; i < npaginas; i++) {
        centro.innerHTML += "<a href='#' data-indice=" + i + ">" + (i + 1) + "</a>";
    }

    /* ********** EJECUTAR PÁGINAS **********   */
 
	var enlaces=document.querySelectorAll(".celda2 a");
    console.log(enlaces);

	enlaces.forEach((va)=>{
		va.addEventListener("click",(event)=>{
            var aux = 0;
            var copia;
			var objeto=event.target;
			var indice=objeto.getAttribute("data-indice"); // a, 0, 1, s
            
            var longitud=fotos.length-1;
            /* para página anterior y siquiete*/
            switch(indice){
                case"a":
                    paginacion=paginacion-1;
                    indice=paginacion;
                    break;

                case "s":
                paginacion=paginacion+1;
                indice=paginacion;

            }
			

            if(parseInt(indice)>=0){
                paginacion=parseInt(indice);
            }

            


            aux=parseInt(indice)*4;
            console.log("paginacion",paginacion);
	        for (var i = 0; i < 4; i++,aux++) {
	               // document.querySelectorAll(".cards")[i].src = "imgs/flowers/thumbs/" + fotos[aux];
                    console.log(aux);
                   if(aux>longitud){
                    document.querySelectorAll(".cards")[i].src="";
                   }else{
                    console.log(aux);
                    document.querySelectorAll(".cards")[i].src =imagenesPeque[aux].src; 
                   } 
	        }

        console.log("paginacion",paginacion);
  
	    });
	});


    /* ********** Precargo imágenes grandes **********  */

    for (var i = 0; i < fotos.length; i++) {
        imagenes[i] = new Image();
        imagenes[i].src = "imgs/flowers/" + fotos[i];
    }

    /* **********  Precargo imágenes pequeñas **********  */

        for(var i=0;i<fotos.length;i++){
            var v;
            imagenesPeque[i]=new Image();
            imagenesPeque[i].src="imgs/flowers/thumbs/" + fotos[i];
        }
        for (var i = 0; i < 4; i++) {
            v=document.querySelectorAll(".cards")[i].src = imagenesPeque[i].src;
        }



    /* ********** imagen por defecto ********** */

    document.querySelector(".mediana").src = imagenes[0].src;

    /* **********  EJECUTAR 1 ********** */

    for (var i = 0; i < 4; i++) {
        document.querySelectorAll(".cards")[i].onclick = ejecutar1;
    }
/* ********** Poner imagen mediana al hacer click **********   */
    function ejecutar1(event) {
        var indice = event.target.getAttribute("data-indice")*1;
        var caja = document.querySelector(".foto");
        indice=paginacion*4+indice;
        caja.style.backgroundImage = "url('" + imagenes[indice].src + "')";
        caja.style.backgroundSize = "cover";
        document.querySelector(".mediana").src = imagenes[indice].src;

    }


    /* ********** Poner imagen grande al hacer click **********   */

    document.querySelector(".foto").onclick = ejecutar2;
    document.querySelector(".ocultar").style.display = "none";
    document.querySelector(".ocultar").onclick = cerrar;

   
    function ejecutar2(event) {
        var indice = event.target.getAttribute("data-indice");
        var caja = document.querySelector(".ocultar");
        caja.style.backgroundImage = "url('" + document.querySelector(".mediana").src + "')";

        caja.style.display = "block";
    }
    /* **********  Cerrar **********  */
    function cerrar() {
        document.querySelector(".ocultar").style.display = "none";
        document.querySelector(".ocultar").style.backgroundImage = "none";
    }


});